package edu.wisc.cs.sdn.sr;

import net.floodlightcontroller.packet.*;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
  * Implements RIP. 
  * @author Anubhavnidhi Abhashkumar and Aaron Gember-Jacobson
  */
public class RIP implements Runnable
{
    public static final int RIP_MULTICAST_IP = 0xE0000009;
    public static final byte[] BROADCAST_MAC = {(byte)0xFF, (byte)0xFF, 
            (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF};
    
    /** Send RIP updates every 10 seconds */
    private static final int UPDATE_INTERVAL = 10;

    /** Timeout routes that neighbors last advertised more than 30 seconds ago*/
    private static final int TIMEOUT = 30;

    /** Router whose route table is being managed */
	private Router router;

    /** Thread for periodic tasks */
    private Thread tasksThread;

	public RIP(Router router)
	{ 
        this.router = router; 
        this.tasksThread = new Thread(this);
    }

    public Ethernet makeRIPPacket(RIPv2 ripPacket, Iface iface, int destIPAddress, byte[] destMacAddress) {
        UDP udp = new UDP();
        udp.setPayload(ripPacket);
        udp.setDestinationPort(UDP.RIP_PORT);
        udp.setSourcePort(UDP.RIP_PORT);
        udp.setChecksum((short) 0);

        IPv4 ipPacket = new IPv4();
        ipPacket.setChecksum((short) 0);
        ipPacket.setPayload(udp);
        ipPacket.setProtocol(IPv4.PROTOCOL_UDP);
        ipPacket.setSourceAddress(iface.getIpAddress());
        ipPacket.setDestinationAddress(destIPAddress);
        ipPacket.setTtl((byte) 64);

        Ethernet etherPacket = new Ethernet();
        etherPacket.setDestinationMACAddress(destMacAddress);
        etherPacket.setSourceMACAddress(iface.getMacAddress().toBytes());
        etherPacket.setEtherType(Ethernet.TYPE_IPv4);
        etherPacket.setPayload(ipPacket);
        return etherPacket;
    }

	public void init()
	{
        // If we are using static routing, then don't do anything
        if (this.router.getRouteTable().getEntries().size() > 0)
        { return; }

        System.out.println("RIP: Build initial routing table");
        for(Iface iface : this.router.getInterfaces().values())
        {
            this.router.getRouteTable().addEntry(
                    (iface.getIpAddress() & iface.getSubnetMask()),
                    0, // No gateway for subnets this router is connected to
                    iface.getSubnetMask(), iface.getName());
        }
        System.out.println("Route Table:\n"+this.router.getRouteTable());

		this.tasksThread.start();

        /*********************************************************************/
        /* TODO: Add other initialization code as necessary                  */
        boardcast();
        /*********************************************************************/
	}

    /**
      * Handle a RIP packet received by the router.
      * @param etherPacket the Ethernet packet that was received
      * @param inIface the interface on which the packet was received
      */
	public void handlePacket(Ethernet etherPacket, Iface inIface)
	{
        // Make sure it is in fact a RIP packet
        if (etherPacket.getEtherType() != Ethernet.TYPE_IPv4)
        { return; } 
		IPv4 ipPacket = (IPv4)etherPacket.getPayload();
        if (ipPacket.getProtocol() != IPv4.PROTOCOL_UDP)
        { return; } 
		UDP udpPacket = (UDP)ipPacket.getPayload();
        if (udpPacket.getDestinationPort() != UDP.RIP_PORT)
        { return; }
		RIPv2 ripPacket = (RIPv2)udpPacket.getPayload();

        /*********************************************************************/
        /* TODO: Handle RIP packet                                           */
        for (RIPv2Entry ripv2Entry : ripPacket.getEntries()) {
            if (ripv2Entry.getMetric() < 16) {
                RouteTableEntry routeTableEntry = router.getRouteTable().findEntry(ripv2Entry.getAddress(), ripv2Entry.getSubnetMask());
                if (routeTableEntry == null) {
                    router.getRouteTable().addEntry(ripv2Entry.getAddress(), ripv2Entry.getNextHopAddress(),
                            ripv2Entry.getSubnetMask(), inIface.getName(), ripv2Entry.getMetric() + 1);
                } else if (ripv2Entry.getMetric() + 1 < routeTableEntry.getCost()) {
                    router.getRouteTable().updateEntry(ripv2Entry.getAddress(), ripv2Entry.getNextHopAddress(),
                            ripv2Entry.getNextHopAddress(), inIface.getName());
                    routeTableEntry.setCost(ripv2Entry.getMetric() + 1);
                }
            }
        }

        /*********************************************************************/
	}
    
    /**
      * Perform periodic RIP tasks.
      */
	@Override
	public void run() 
    {
        /*********************************************************************/
        /* TODO: Send period updates and time out route table entries        */
        for (;;) {
            try {
                Thread.sleep(UPDATE_INTERVAL * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (router.getRouteTable().getEntries()) {
		/*
                for (ListIterator<RouteTableEntry> iter = router.getRouteTable().getEntries().listIterator(); iter.hasNext(); ) {
                    RouteTableEntry entry = iter.next();

                    System.out.println("inter face: " + entry.getInterface());
                    System.out.println("destination: " + entry.getDestinationAddress());
                    System.out.println("gateway: " + entry.getGatewayAddress());
                    System.out.println("cost: " + entry.getCost());

                }*/

                for (ListIterator<RouteTableEntry> iter = router.getRouteTable().getEntries().listIterator(); iter.hasNext(); ) {
                    RouteTableEntry entry = iter.next();
                    if (entry.getCost() != 0 && System.currentTimeMillis() - entry.getTime() >= RIP.TIMEOUT * 1000) {
                        iter.remove();
                    }
                }
            }
            boardcast();
        }

        /*********************************************************************/
	}

    void boardcast() {
        for (Iface iface: this.router.getInterfaces().values()) {
            RIPv2 ripv2 = new RIPv2();
            ripv2.setCommand(RIPv2.COMMAND_REQUEST);
            List<RIPv2Entry> waitSending = new LinkedList<RIPv2Entry>();
            for (RouteTableEntry iter: router.getRouteTable().getEntries()) {
                RIPv2Entry ripv2Entry = iter.toRIPv2Entry();
                ripv2Entry.setNextHopAddress(iface.getIpAddress());
                waitSending.add(ripv2Entry);
            }
            ripv2.setEntries(waitSending);
            router.sendPacket(makeRIPPacket(ripv2, iface, RIP_MULTICAST_IP, BROADCAST_MAC), iface);
        }
    }
}
